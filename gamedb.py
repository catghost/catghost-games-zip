#!/usr/bin/env python

import datetime, hashlib, os, os.path, shutil, sys, zipfile

BLOCKSIZE = 4096
TIMESTAMP_FORMAT = '%Y-%m-%d %H:%M:%S'

def md5sum_file(fp):
    """ Gets the MD5 sum of a file's data. """
    md5 = hashlib.md5()
    while True:
        data = fp.read(BLOCKSIZE)
        md5.update(data)
        if len(data) != BLOCKSIZE:
            return (md5.digest(), md5.hexdigest())

def md5sum_path(filepath):
    """ Gets the MD5 sum of a file's contents given a filepath. """
    with open(filepath, 'rb') as fp:
        return md5sum_file(fp)

def get_zipfile_datetime(filepath, subfile='data.win'):
    """ Gets the zipfile datetime of an archived file. """
    with zipfile.ZipFile(filepath) as gamezip:
         infolist = gamezip.infolist()
         info = next(iter([x for x in infolist if x.filename == subfile]), None)
         if info is not None:
             dt = datetime.datetime(info.date_time[0], info.date_time[1], info.date_time[2],
                     info.date_time[3], info.date_time[4], info.date_time[5])
             return (info.date_time, dt)

class GameVersionInfo(object):
    """ Game version information. """
    def __init__(self, md5sum, timestamp):
        self._md5sum = md5sum
        self._timestamp = timestamp

    @property
    def md5sum(self):
        return self._md5sum

    @property
    def md5sum_hex(self):
        """ Gets a hex string representation of the md5sum. """
        return ''.join(['{0:02x}'.format(x) for x in self.md5sum])

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def timestamp_str(self):
        """ Gets the timestamp as a formatted string. """
        return self.timestamp.strftime(TIMESTAMP_FORMAT)

    def to_object(self):
        """ Returns an object. """
        return { 'md5sum': self.md5sum_hex, 'timestamp': self.timestamp_str }

    @staticmethod
    def from_object(obj):
        """ Gets a GameVersionInfo from an object. """
        md5sum = bytes.fromhex(obj['md5sum'])
        timestamp = datetime.datetime.strptime(obj['timestamp'], TIMESTAMP_FORMAT)
        return GameVersionInfo(md5sum=md5sum, timestamp=timestamp)

class GameInfo(object):
    """ Info about a specific game, including versions. """
    def __init__(self, name, archive, versions=[]):
        self._name = name
        self._archive = archive
        self._versions = versions

    @property
    def name(self):
        return self._name

    @property
    def archive(self):
        return self._archive

    @property
    def versions(self):
        return tuple(self._versions[:])

    def add_version(self, version_info):
        """ Returns a new GameInfo with an added GameVersionInfo. """
        versions = self.versions + (version_info,)
        return GameInfo(self.name, self.archive, versions)

    def update_version(self, version_info):
        """ Updates an existing GameVersionInfo, returning a new GameInfo. """
        versions = [version_info if x.md5sum.lower() == version_info.md5sum.lower() else x for x in self.versions]
        gameinfo = GameInfo(name=self.name, archive=self.archive, versions=versions)
        return (gameinfo, version_info in versions)

    def to_object(self):
        """ Returns an object. """
        versions = [x.to_object() for x in self.versions]
        return { 'name': self.name, 'archive': self.archive, 'versions': versions }

    @staticmethod
    def from_object(obj):
        """ Builds a GameInfo from an object. """
        versions = tuple([GameVersionInfo.from_object(x) for x in obj['versions']])
        return GameInfo(name=obj['name'], archive=obj['archive'], versions=versions)

class GamesDatabase(object):
    """ Games database. """
    def __init__(self, games=[]):
        self._games = games

    @property
    def games(self):
        """ Gets a copy of the games list. """
        return tuple(self._games[:])

    def add_game(self, gameinfo):
        """ Returns a new GamesDatabase with an added GameInfo. """
        return GamesDatabase(self.games + (gameinfo,))

    def update_game(self, gameinfo):
        """ Updates an existing GameInfo, returning a new GamesDatabase. """
        games = tuple([x if x.name != gameinfo.name else gameinfo for x in self.games])
        if gameinfo in games:
            return GamesDatabase(games)

    def to_object(self):
        """ Returns an object. """
        return [x.to_object() for x in self.games]

    @staticmethod
    def from_object(obj):
        """ Builds a GamesDatabase from an object. """
        games = [GameInfo.from_object(x) for x in obj]
        return GamesDatabase(games=games)

def perform_add(args):
    """ Adds a game to the games directory and re-scans the directory. """
    # Make sure this is a valid zip file
    with zipfile.ZipFile(args.add) as temp:
        pass
    (digest, hexdigest) = md5sum_path(args.add)
    dirpath = os.path.join(args.games_directory, hexdigest.lower())
    try: os.mkdir(dirpath)
    except FileExistsError:
        print('warning: hash directory already exists:', dirpath, file=sys.stdout)
    destpath = os.path.join(dirpath, os.path.basename(args.add))
    if args.move:
        shutil.move(args.add, destpath)
    else:
        shutil.copy(args.add, destpath)
    return perform_scan(args, adding_hash=digest)

def perform_display(args):
    """ Displays info given a games JSON database file. """
    # Read database JSON into class
    with open(args.file, 'r') as fp:
        database = GamesDatabase.from_object(json.load(fp))
    # Print info about database
    print()
    for game in database.games:
        print('{0} ({1}):'.format(game.name, game.archive))
        for version in game.versions:
            print(' {0} : {1}'.format(version.md5sum_hex, version.timestamp_str))
        print()
    return 0

def perform_scan(args, adding_hash=None):
    """ Scans a directory and outputs a game JSON database from scanned games. """
    data = []
    for dirname in os.listdir(args.games_directory):
        dirpath = os.path.join(args.games_directory, dirname)
        child = next(iter(os.listdir(dirpath)), None)
        if child is None:
            print('error: expected exactly one file in hash directory:', dirpath, file=sys.stderr)
            return 1
        childpath = os.path.join(dirpath, child)
        (digest, hexdigest) = md5sum_path(childpath)
        if hexdigest.lower() != dirname.lower():
            print('error: directory name is not hash of file contents:', childpath, file=sys.stderr)
            return 1
        (_, timestamp) = get_zipfile_datetime(childpath)
        if timestamp is None:
            print('error: unable to find date_time tuple of data.win file in zip archive:', childpath, file=sys.stderr)
            return 1
        version = GameVersionInfo(md5sum=digest, timestamp=timestamp)
        data.append((child, version))

    with open(args.file, 'r') as fp:
        database = GamesDatabase.from_object(json.load(fp))

    for d in data:
        existing = next(iter([x for x in database.games if d[0] == x.archive]), None)
        if existing is not None:
            (existing, updated) = existing.update_version(d[1])
            if not updated:
                existing = existing.add_version(d[1])
            database = database.update_game(existing)
        elif adding_hash == d[1].md5sum:
            name = os.path.splitext(d[0])[0].lower()
            info = GameInfo(name=name, archive=d[0], versions=(d[1],))
            database = database.add_game(info)
        else:
            print('warning: no existing game found for archive:', d[0], file=sys.stderr)

    # Dump new database JSON to original file or stdout
    if args.overwrite:
        with open(args.file, 'w') as fp:
            json.dump(database.to_object(), fp=fp, indent=2)
            print('', file=fp)
    else:
        json.dump(database.to_object(), fp=sys.stdout, indent=2)

    return 0

if __name__ == '__main__':
    import argparse, json

    def get_parser():
        parser = argparse.ArgumentParser(description='Script for manipulating the game files and JSON database.')
        parser.add_argument('-a', '--add',
                help='path to game zip file to add')
        parser.add_argument('-d', '--games-directory', default='games', metavar='DIRECTORY',
                help='path to games directory')
        parser.add_argument('-f', '--file', default='games.json',
                help='path to games JSON file')
        parser.add_argument('-s', '--scan', action='store_true',
                help='store the games directory and output a games JSON database')
        parser.add_argument('--move', action='store_true',
                help='move the game file instead of copying when adding a game')
        parser.add_argument('--overwrite', action='store_true',
                help='overwrite games JSON database file instead of printing to stdout')
        return parser

    args = get_parser().parse_args()

    if args.add:
        exit(perform_add(args))
    elif args.scan:
        exit(perform_scan(args))
    else:
        exit(perform_display(args))
