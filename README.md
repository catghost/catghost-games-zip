catghost-games-zip
==================

This is a database of all known released [CatGhost] game zip files, organized
by md5sum.

The `games.json` file contains meta-information about each game, including
timestamps of when the games were last modified according to the zip files.


Script Usage
------------

The `gamedb.py` script can be used to regenerate the `games.json` database, and
add a game to the games directory and database.

```sh
# To scan the games directory and regenerate:
> ./gamedb.py -s --overwrite

# If you exclude --overwrite, it will write to stdout instead:
> ./gamedb.py -s

# To add a new game to the games directory and database:
> ./gamedb.py -a new_game.zip --overwrite
```

[CatGhost]:https://www.youtube.com/channel/UCwdmPCVOhTW2u_fKpxRLMdA

